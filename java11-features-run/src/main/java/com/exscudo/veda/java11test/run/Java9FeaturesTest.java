package com.exscudo.veda.java11test.run;

import com.exscudo.veda.java11test.module1.*;
import com.exscudo.veda.java11test.module1.model.*;
import com.exscudo.veda.java11test.module2.*;
import com.exscudo.veda.java11test.run.flow.*;
import org.apache.commons.lang3.tuple.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;

/**
 *
 */
public class Java9FeaturesTest {

    public static void main(String[] args)
            throws Exception {
        feature_Java9_ImmutableCollections();
        feature_Java9_privateMethodsInInterface();
        feature_Java9_ModuleSystem();
        feature_Java9_ProcessHandle();
        feature_Java9_TryWithResource();
        feature_Java9_CompletableFuture();
        feature_Java9_ReactiveStreamsFlow();
        feature_Java9_DiamondAnonymous();
        feature_Java9_Optional();
        feature_Java9_Stream();
        feature_Java9_Deprecated();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_ImmutableCollections() {
        System.out.println("------ Java9 ------ ImmutableCollections --------------------------------------");

        List<String> emptyImmutableList = List.of();
        List<String> singletonImmutableList = List.of("one");
        List<String> simpleImmutableList = List.of("one", "two", "three");
        System.out.println(emptyImmutableList);
        System.out.println(singletonImmutableList);
        System.out.println(simpleImmutableList);

        Set<String> emptyImmutableSet = Set.of();
        Set<String> singletonImmutableSet = Set.of("one");
        Set<String> simpleImmutableSet = Set.of("one", "two", "three");
        System.out.println(emptyImmutableSet);
        System.out.println(singletonImmutableSet);
        System.out.println(simpleImmutableSet);

        Map<String, String> emptyImmutableMap = Map.of();
        Map<String, String> singletonImmutableMap = Map.of("key1", "value1");
        Map<String, String> maxLengthImmutableMap = Map.of(
                "key1", "value1",
                "key2", "value2",
                "key3", "value3",
                "key4", "value4",
                "key5", "value5",
                "key6", "value6",
                "key7", "value7",
                "key8", "value8",
                "key9", "value9",
                "key10", "value10");
        Map<String, String> simpleImmutableMap = Map.ofEntries(
                new ImmutablePair<>("key1", "value1"),
                new ImmutablePair<>("key2", "value2"),
                new ImmutablePair<>("key3", "value3"),
                new ImmutablePair<>("key4", "value4"),
                new ImmutablePair<>("key5", "value5"),
                new ImmutablePair<>("key6", "value6"));
        System.out.println(emptyImmutableMap);
        System.out.println(singletonImmutableMap);
        System.out.println(maxLengthImmutableMap);
        System.out.println(simpleImmutableMap);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_privateMethodsInInterface() {
        System.out.println("------ Java9 ------ PrivateMethodsInInterface ---------------------------------");
        Feature_Java9_PrivateMethodsInInterface ref = new Feature_Java9_PrivateMethodsInInterface() {
        };
        int result = ref.defaultMethod(7);
    }

    public interface Feature_Java9_PrivateMethodsInInterface {

        default int defaultMethod(int value) {
            int result = 10 + this.privateInterfaceMethod(value);
            System.out.println("defaultMethod(" + value + ") == " + result);
            return result;
        }

        private int privateInterfaceMethod(int value) {
            int result = value * Feature_Java9_PrivateMethodsInInterface.privateStaticMethod(value * value);
            System.out.println("privateDefaultMethod(" + value + ") == " + result);
            return result;
        }

        private static int privateStaticMethod(int value) {
            int result = value % 5;
            System.out.println("privateStaticMethod(" + value + ") == " + result);
            return result;
        }

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_ModuleSystem() {
        System.out.println("------ Java9 ------ ModuleSystem ----------------------------------------------");
        ModuleOne moduleOne = ModuleOne.getInstance();
        ModuleTwo moduleTwo = ModuleTwo.getInstance(moduleOne);

        // Class 'Data' is imported from 'java11_test_moduleOne' module.
        List<Data> dataList = moduleTwo.generateData(System.currentTimeMillis());
        for (Data data : dataList) {
            System.out.println(data);
        }

        // Compile error: package com.exscudo.veda.java11test.module1.impl is not visible
        // ModuleOne moduleOneImpl = new com.exscudo.veda.java11test.module1.impl.ModuleOneImpl();
        // Compile error: package com.exscudo.veda.java11test.module2.impl is not visible
        // ModuleTwo moduleTwoImpl = new com.exscudo.veda.java11test.module2.impl.ModuleTwoImpl(moduleOne);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_ProcessHandle()
            throws Exception {
        System.out.println("------ Java9 ------ ProcessHandle ---------------------------------------------");
        ProcessHandle current = ProcessHandle.current();

        System.out.println("JVM PID: " + current.pid());
        System.out.println("JVM Info: " + current.info());
        System.out.println("JVM Descendants: " + current.descendants().collect(Collectors.toList()));
        System.out.println("JVM Children: " + current.children().collect(Collectors.toList()));
        System.out.println("JVM SupportsNormalTermination: " + current.supportsNormalTermination());
        Optional<ProcessHandle> optionalParent = current.parent();
        if (optionalParent.isPresent()) {
            ProcessHandle parent = optionalParent.get();
            System.out.println("JVM Parent PID: " + parent.pid());
            System.out.println("JVM Parent Info: " + parent.info());
        } else {
            System.out.println("JVM Parent absent.");
        }

        List<Long> processList = ProcessHandle.allProcesses()
                .map(ProcessHandle::pid)
                .sorted()
                .collect(Collectors.toList());
        System.out.println("All PIDs: " + processList);

        new ProcessBuilder()
                .command("ls")
                .start()
                .onExit().thenAccept(p -> System.out.println("Completed process PID: " + p.pid()))
                .get();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_TryWithResource()
            throws Exception {
        System.out.println("------ Java9 ------ TryWithResource -------------------------------------------");
        StringReader reader = new StringReader("Some string.");
        try (reader) {
            System.out.println("First char: " + (char) reader.read());
        }
        try {
            System.out.println("Second char: " + (char) reader.read());

        } catch (IOException ex) {
            System.out.println("Stream is already closed.");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_CompletableFuture()
            throws Exception {
        System.out.println("------ Java9 ------ CompletableFuture -----------------------------------------");
        ExecutorService executor = Executors.newFixedThreadPool(1);
        try {
            // Delayed executor
            System.out.println("Schedule simple job...");
            Executor delayedExecutor = CompletableFuture
                    .delayedExecutor(1, TimeUnit.SECONDS, executor);
            delayedExecutor.execute(() -> System.out.println("Executed something."));
            delayedExecutor.execute(() -> System.out.println("Executed other thing."));
            System.out.println("Simple job is scheduled.");

            // Completed future.
            CompletableFuture<String> completedFuture = CompletableFuture
                    .completedFuture("Already COMPLETED.");
            System.out.println("Completed future: " + completedFuture.get());

            // Failed future.
            CompletableFuture<Object> failedFuture = CompletableFuture
                    .failedFuture(new RuntimeException("Something has failed."));
            try {
                System.out.println("Failed future object: " + failedFuture.get());
            } catch (ExecutionException ex) {
                System.out.println("Failed future exception: " + ex.getMessage());
            }

            // Async supplier.
            CompletableFuture<String> supplyAsync = CompletableFuture
                    .supplyAsync(() -> "Async supplied value: " + System.currentTimeMillis(), delayedExecutor);
            System.out.println("Async started at: " + System.currentTimeMillis());
            System.out.println("Async result: " + supplyAsync.get());


            Thread.sleep(1500);
        } finally {
            System.out.println("Destroy executor.");
            executor.shutdown();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_ReactiveStreamsFlow()
            throws Exception {
        System.out.println("------ Java9 ------ ReactiveStreamsFlow ---------------------------------------");
        List<Integer> firstList = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        Map<Integer, String> transformationMap = Map.of(
                1, "one", 2, "two", 3, "three", 4, "four", 5, "five", 6, "six", 7, "seven", 8, "eight");
        CountDownLatch firstCount = new CountDownLatch(firstList.size());
        CountDownLatch secondCount = new CountDownLatch(firstList.size());

        ExecutorService executor = Executors.newFixedThreadPool(2);
        try {
            SubmissionPublisher<Integer> publisher = new SubmissionPublisher<>(executor, 2);
            TestProcessor transformer = new TestProcessor(executor, 1, firstCount, transformationMap);
            TestSubscriber consumer = new TestSubscriber(secondCount);
            try (publisher;
                 transformer) {
                // Build processing sequence.
                publisher.subscribe(transformer);
                transformer.subscribe(consumer);
                // Feed values.
                for (Integer value : firstList) {
                    publisher.submit(value);
                    System.out.println(">>> " + value + " : " + Thread.currentThread().getName());
                }
                // Wait for processing of all values.
                firstCount.await();
                secondCount.await();
                // Close publishers.
            }
        } finally {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.SECONDS);
        }
        System.out.println("All tasks are done.");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_DiamondAnonymous()
            throws Exception {
        System.out.println("------ Java9 ------ DiamondAnonymous ------------------------------------------");
        List<Number> someList = new ArrayList<>(List.of(123, 456)) {
            @Override
            public Number get(int index) {
                return super.get(index);
            }
        };
        System.out.println(someList);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_Optional()
            throws Exception {
        System.out.println("------ Java9 ------ Optional --------------------------------------------------");
        List<String> list = List.of("test", "value")
                .stream()
                .filter("test"::equals)
                .findFirst()
                .stream() // New in Java 9.
                .collect(Collectors.toList());
        System.out.println(list);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java9_Stream()
            throws Exception {
        System.out.println("------ Java9 ------ Stream ----------------------------------------------------");
        List<Integer> takeWhileList = List
                .of(1, 2, 7, 3, 4, 5, 6, 8, 9, 10)
                .stream()
                .takeWhile(x -> x < 4)
                .collect(Collectors.toList());
        System.out.println("takeWhile: " + takeWhileList);

        List<Integer> dropWhileList = List
                .of(1, 2, 7, 3, 4, 5, 6, 8, 9, 10)
                .stream()
                .dropWhile(x -> x < 4)
                .collect(Collectors.toList());
        System.out.println("dropWhile: " + dropWhileList);

        IntStream
                .iterate(2, x -> x < 20, x -> x * x)
                .forEach(System.out::println);

        // Out of box infinite loop.
//        IntStream
//                .iterate(2, x -> x + 1)
//                .filter(x -> x > 20)
//                .forEach(System.out::println);
        IntStream
                .iterate(2, x -> x + 1)
                .filter(x -> x > 20)
                .findFirst()
                .ifPresent(System.out::println);

        boolean isNullableEmpty = Stream.<String>ofNullable(null)
                .findFirst()
                .isEmpty();
        System.out.println(isNullableEmpty);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    @Deprecated(since = "1917-11-07", forRemoval = true)
    public static void feature_Java9_Deprecated()
            throws Exception {
        System.out.println("------ Java9 ------ Deprecated ------------------------------------------------");
    }

}