package com.exscudo.veda.java11test.run.flow;

import java.util.concurrent.*;

/**
 *
 */
public class TestSubscriber
        implements Flow.Subscriber<String> {

    private final CountDownLatch count;

    private volatile Flow.Subscription subscription;

    public TestSubscriber(CountDownLatch count) {
        this.count = count;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        System.out.println("Subscribed consumer: " + Thread.currentThread().getName());
        this.subscription.request(1);
    }

    @Override
    public void onNext(String value) {
        System.out.println(value + " <<< : " + Thread.currentThread().getName());
        this.subscription.request(1);
        this.count.countDown();
        //throw new RuntimeException("Some Error!");
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
        while (this.count.getCount() > 0) {
            this.count.countDown();
        }
    }

    @Override
    public void onComplete() {
        System.out.println("Completed consumer: " + Thread.currentThread().getName());
    }

}