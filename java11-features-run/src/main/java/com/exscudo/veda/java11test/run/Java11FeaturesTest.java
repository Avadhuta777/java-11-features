package com.exscudo.veda.java11test.run;

import java.net.*;
import java.net.http.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 *
 */
public class Java11FeaturesTest {

    public static void main(String[] args)
            throws Exception {
        feature_Java11_String();
        feature_Java11_VariablesOfUnknownTypeInLambda();
        feature_Java11_JavaEERemoved();
        feature_Java11_HttpClient();
        feature_Java11_ReadWriteStringFromFile();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java11_String() {
        System.out.println("------ Java11 ------ String ---------------------------------------------------");
        System.out.println("Empty: " + " \t\n".isEmpty());
        System.out.println("Blank: " + " \t\n".isBlank());
        System.out.println("Lines: " + "1\n2\n3".lines().collect(Collectors.toList()));
        System.out.println("Lines: " + "1\n2\n3\n".lines().collect(Collectors.toList()));
        System.out.println("Strip: '" + " \tABC\n  ".strip() + "'");
        System.out.println("Repeat".repeat(3));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java11_VariablesOfUnknownTypeInLambda() {
        System.out.println("------ Java11 ------ VariablesOfUnknownTypeInLambda ---------------------------");
        BinaryOperator<Integer> operator1 = (var i, var j) -> (i + j) * 2;
        System.out.println("Var type: " + operator1.apply(2, 5));

        BinaryOperator<Integer> operator2 = (i, j) -> (i + j) * 2;
        System.out.println("No type: " + operator2.apply(2, 5));

        BinaryOperator<Integer> operator3 = (Integer i, Integer j) -> (i + j) * 2;
        System.out.println("Explicit type: " + operator3.apply(2, 5));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java11_JavaEERemoved() {
        System.out.println("------ Java11 ------ JavaEERemoved --------------------------------------------");
//        <sun-jaxb.version>2.3.0</sun-jaxb.version>
//        <javax.activation.version>1.1.1</javax.activation.version>
//        <net.sf.saxon.version>9.9.1-6</net.sf.saxon.version>

//            <!-- XML support for Java 11 -->
//            <dependency>
//                <groupId>com.sun.xml.bind</groupId>
//                <artifactId>jaxb-core</artifactId>
//                <version>${sun-jaxb.version}</version>
//            </dependency>
//            <dependency>
//                <groupId>com.sun.xml.bind</groupId>
//                <artifactId>jaxb-impl</artifactId>
//                <version>${sun-jaxb.version}</version>
//            </dependency>
//            <dependency>
//                <groupId>javax.activation</groupId>
//                <artifactId>activation</artifactId>
//                <version>${javax.activation.version}</version>
//            </dependency>
//            <dependency>
//                <groupId>net.sf.saxon</groupId>
//                <artifactId>Saxon-HE</artifactId>
//                <version>${net.sf.saxon.version}</version>
//            </dependency>

//            <dependency>
//                <groupId>javax.transaction</groupId>
//                <artifactId>javax.transaction-api</artifactId>
//                <version>1.3</version>
//            </dependency>
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java11_HttpClient()
            throws Exception {
        System.out.println("------ Java11 ------ HttpClient -----------------------------------------------");
        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest getRequest = HttpRequest.newBuilder(URI.create("https://ya.ru"))
                .GET()
                .header("Accept-Charset", "UTF-8")
                .build();

        HttpResponse<String> response = httpClient.send(
                getRequest, HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
        System.out.println("content-type: " + response.headers().firstValue("content-type"));
        System.out.println(response.body().substring(0, 50));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java11_ReadWriteStringFromFile()
            throws Exception {
        System.out.println("------ Java11 ------ HttpClient -----------------------------------------------");
        Path tempFile = Files.createTempFile("feature_Java11_ReadWriteStringFromFile", ".txt");
        System.out.println("Temp file: " + tempFile);
        Files.writeString(tempFile, "Some string!\nOther string?\n",
                StandardCharsets.UTF_8, StandardOpenOption.WRITE);
        Files.writeString(tempFile, "Some string!\nOther string?'n",
                StandardCharsets.UTF_8, StandardOpenOption.APPEND);

        List<String> lines = Files.readAllLines(tempFile, StandardCharsets.UTF_8);
        System.out.println("Lines: " + lines);
    }

}