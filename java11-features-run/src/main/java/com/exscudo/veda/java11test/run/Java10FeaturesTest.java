package com.exscudo.veda.java11test.run;

import java.util.*;
import java.util.stream.*;

/**
 *
 */
public class Java10FeaturesTest {

    public static void main(String[] args)
            throws Exception {
        feature_Java10_VariablesOfUnknownType();
        feature_Java10_CollectionCopyOf();
        feature_Java10_OptionalOrElseThrow();
        feature_Java10_CollectorToUnmodifiable();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java10_VariablesOfUnknownType() {
        System.out.println("------ Java10 ------ VariablesOfUnknownType -----------------------------------");
        var var = List.of(1, 2, 3); // Inferred ArrayList<String> value???
        for (var number : var) {    // No! It's inferred Integer value.
            System.out.println(number);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java10_CollectionCopyOf() {
        System.out.println("------ Java10 ------ CollectionCopyOf -----------------------------------------");
        ArrayList<String> list = new ArrayList<>();
        list.add("123");
        list.add("456");
        List<String> listCopy = List.copyOf(list);
        System.out.println("List copy: " + listCopy);
        Set<String> setCopy = Set.copyOf(list);
        System.out.println("Set copy: " + setCopy);
        Map<Object, Object> mapCopy = Map.copyOf(Map.of(1, "2", 4, "4"));
        System.out.println("Map copy: " + mapCopy);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java10_OptionalOrElseThrow()
            throws Exception {
        System.out.println("------ Java10 ------ OptionalOrElseThrow --------------------------------------");
        try {
            String test = List.of("some", "value")
                    .stream()
                    .filter("test"::equals)
                    .findFirst()
                    .get();
        } catch (NoSuchElementException ex) {
            System.out.println("Failed get()");
        }
        try {
            String test = List.of("some", "value")
                    .stream()
                    .filter("test"::equals)
                    .findFirst()
                    .orElseThrow();
        } catch (NoSuchElementException ex) {
            System.out.println("Failed orElseThrow()");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java10_CollectorToUnmodifiable()
            throws Exception {
        System.out.println("------ Java10 ------ CollectorToUnmodifiable ----------------------------------");
        Set<String> unmodifiableSet = List.of("some", "value")
                .stream()
                .collect(Collectors.toUnmodifiableSet());
        try {
            unmodifiableSet.clear();
        } catch (UnsupportedOperationException ex) {
            System.out.println("Unmodifiable set!");
        }
    }

}