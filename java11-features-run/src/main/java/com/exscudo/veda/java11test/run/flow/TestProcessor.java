package com.exscudo.veda.java11test.run.flow;

import java.util.*;
import java.util.concurrent.*;

/**
 *
 */
public class TestProcessor
        extends SubmissionPublisher<String>
        implements Flow.Processor<Integer, String> {

    private final CountDownLatch count;
    private final Map<Integer, String> transformationMap;
    private volatile Flow.Subscription subscription;

    public TestProcessor(Executor executor, int maxBufferCapacity, CountDownLatch count,
            Map<Integer, String> transformationMap) {
        super(executor, maxBufferCapacity);
        this.count = count;
        this.transformationMap = transformationMap;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        System.out.println("Subscribed processor: " + Thread.currentThread().getName());
        this.subscription.request(1);
    }

    @Override
    public void onNext(Integer value) {
        System.out.println(value + " <<< : " + Thread.currentThread().getName());
        String transformed = this.transformationMap.get(value);
        if (transformed == null) {
            throw new IllegalArgumentException("Invalid value: " + value);
        }
        System.out.println(">>> " + transformed + " : " + Thread.currentThread().getName());
        this.submit(transformed);
        this.subscription.request(1);
        this.count.countDown();
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
        while (this.count.getCount() > 0) {
            this.count.countDown();
        }
    }

    @Override
    public void onComplete() {
        System.out.println("Completed processor: " + Thread.currentThread().getName());
    }

}