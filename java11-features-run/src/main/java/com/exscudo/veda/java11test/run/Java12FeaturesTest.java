package com.exscudo.veda.java11test.run;

import java.io.*;
import java.nio.file.*;
import java.text.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 *
 */
public class Java12FeaturesTest {

    public static void main(String[] args)
            throws Exception {
        feature_Java12_fileMismatch();
        feature_Java12_compactNumberFormatting();
        feature_Java12_streamCollectorTeeing();
        feature_Java12_stringIndentation();
        feature_Java12_transform();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void feature_Java12_fileMismatch()
            throws Exception {
        System.out.println("------ Java12 ------ fileMismatch ---------------------------------------------");
        File file1 = File.createTempFile("TestFile1-", ".txt");
        File file2 = File.createTempFile("TestFile2-", ".txt");
        Files.writeString(file1.toPath(), "Some Thing.");
        Files.writeString(file2.toPath(), "Some Think.");
        file1.deleteOnExit();
        file2.deleteOnExit();

        long mismatch = Files.mismatch(file1.toPath(), file2.toPath());
        System.out.println("Mismatch: " + mismatch);
    }

    public static void feature_Java12_compactNumberFormatting()
            throws Exception {
        System.out.println("------ Java12 ------ compactNumberFormatting ----------------------------------");
        NumberFormat shortCompactEN = NumberFormat.getCompactNumberInstance(
                new Locale("en", "EN"), NumberFormat.Style.SHORT);
        shortCompactEN.setMaximumFractionDigits(1);
        System.out.println("shortCompactEN: " + shortCompactEN.format(2591532L));

        NumberFormat longCompactEN = NumberFormat.getCompactNumberInstance(
                new Locale("en", "EN"), NumberFormat.Style.LONG);
        longCompactEN.setMaximumFractionDigits(2);
        System.out.println("longCompactEN: " + longCompactEN.format(2591532L));

        NumberFormat shortCompactRU = NumberFormat.getCompactNumberInstance(
                new Locale("ru", "RU"), NumberFormat.Style.SHORT);
        shortCompactRU.setMaximumFractionDigits(1);
        System.out.println("shortCompactRU: " + shortCompactRU.format(2591L));

        NumberFormat longCompactRU = NumberFormat.getCompactNumberInstance(
                new Locale("ru", "RU"), NumberFormat.Style.LONG);
        longCompactRU.setMaximumFractionDigits(2);
        System.out.println("longCompactRU: " + longCompactRU.format(2591L));
    }

    public static void feature_Java12_streamCollectorTeeing()
            throws Exception {
        System.out.println("------ Java12 ------ streamCollectorTeeing ------------------------------------");
        Integer value = Stream
                .of(11, 12, 13, 14, 15)
                .collect(Collectors.teeing(
                        Collectors.maxBy(Comparator.comparingInt(i -> i)),
                        Collectors.minBy(Comparator.comparingInt(i -> i)),
                        (max, min) -> max.get() - min.get()
                ));
        System.out.println("Min and Max difference: " + value);
    }

    public static void feature_Java12_stringIndentation()
            throws Exception {
        System.out.println("------ Java12 ------ stringIndentation ----------------------------------------");
        String formattedPrettyText = "" +
                "This\n" +
                "  TEXT\r" +
                "    has very\n" +
                "    COOL\n" +
                "formatting\n" +
                "  !!!";
        System.out.println(formattedPrettyText);
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println(formattedPrettyText.indent(4));
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println(formattedPrettyText.indent(-2));
        System.out.println("-------------------------------------------------------------------------------");
    }

    public static void feature_Java12_transform()
            throws Exception {
        System.out.println("------ Java12 ------ transform ------------------------------------------------");
        Function<String, String> truncFunction = (str) -> {
            if (str.length() > 4) {
                return str.substring(4);
            } else {
                return str;
            }
        };

        String source = "This is text with many letters.";
        System.out.println(source);

        String result1 = source.transform(truncFunction);
        System.out.println(result1);

        String result2 = truncFunction.apply(source);
        System.out.println(result2);

        String result3 = "This is text with many letters."
                .transform(truncFunction)
                .transform(truncFunction)
                .transform(truncFunction);
        System.out.println(result3);
    }

}