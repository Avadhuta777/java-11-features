module java11_test_run {

    requires java.net.http;
    requires org.apache.commons.lang3;

    requires java11_test_moduleOne;
    requires java11_test_moduleTwo;

}