package com.exscudo.veda.java11test.module1;

import com.exscudo.veda.java11test.module1.impl.*;
import com.exscudo.veda.java11test.module1.model.*;

/**
 *
 */
public interface ModuleOne {

    static ModuleOne getInstance() {
        return new ModuleOneImpl();
    }

    Data getData(String content);

}