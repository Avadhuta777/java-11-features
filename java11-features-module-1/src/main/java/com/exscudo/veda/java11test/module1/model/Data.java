package com.exscudo.veda.java11test.module1.model;

/**
 *
 */
public class Data {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Data{" +
                "content='" + content + '\'' +
                '}';
    }

}