package com.exscudo.veda.java11test.module1.impl;

import com.exscudo.veda.java11test.module1.*;
import com.exscudo.veda.java11test.module1.model.*;

/**
 *
 */
public class ModuleOneImpl
        implements ModuleOne {

    @Override
    public Data getData(String content) {
        Data data = new Data();
        data.setContent("Content created by ModuleOne: " + content);
        return data;
    }

}