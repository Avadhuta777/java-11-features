module java11_test_moduleTwo {

    requires org.apache.commons.lang3;
    requires java11_test_moduleOne;

    exports com.exscudo.veda.java11test.module2;

}