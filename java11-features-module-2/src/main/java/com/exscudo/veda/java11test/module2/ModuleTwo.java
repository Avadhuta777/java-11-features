package com.exscudo.veda.java11test.module2;

import com.exscudo.veda.java11test.module1.*;
import com.exscudo.veda.java11test.module1.model.*;
import com.exscudo.veda.java11test.module2.impl.*;

import java.util.*;

/**
 *
 */
public interface ModuleTwo {

    static ModuleTwo getInstance(ModuleOne moduleOne) {
        return new ModuleTwoImpl(moduleOne);
    }

    List<Data> generateData(long timestamp);

}