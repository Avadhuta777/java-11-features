package com.exscudo.veda.java11test.module2.impl;

import com.exscudo.veda.java11test.module1.*;
import com.exscudo.veda.java11test.module1.model.*;
import com.exscudo.veda.java11test.module2.*;
import org.apache.commons.lang3.time.*;

import java.util.*;

/**
 *
 */
public class ModuleTwoImpl
        implements ModuleTwo {

    private final ModuleOne moduleOne;

    public ModuleTwoImpl(ModuleOne moduleOne) {
        this.moduleOne = moduleOne;
    }

    @Override
    public List<Data> generateData(long timestamp) {
        return List.of(
                this.moduleOne.getData("Start of ModuleTwo content."),
                this.moduleOne.getData(FastDateFormat
                        .getInstance("dd.MM.yyyy HH:mm z", TimeZone.getTimeZone("UTC"))
                        .format(timestamp)),
                this.moduleOne.getData("End of ModuleTwo content.")
        );
    }

}